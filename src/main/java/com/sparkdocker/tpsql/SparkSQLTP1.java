package com.sparkdocker.tpsql;

import static org.apache.spark.sql.functions.col;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.sparkdocker.entite.Decompte;

public class SparkSQLTP1 {

	private static final String INPUT_DIR = "input.dir";
	private static final String OUTPUT_DIR = "output.dir";
	
	// Classe d'âge de l'assuré
	private static final int ID_AGE_BEN_SNDS = 2;
	// Nature de la prestation
	private static final int ID_PRS_NAT = 39;
	// Montant de la prestation
	private static final int ID_PRS_PAI_MNT = 20;
	// Delimiter
	private static final String DELIMITER = ",";
	
	public static JavaSparkContext sc = null;
	
	private static boolean windowsUser = false;
	private static String output_dir;
	
	public static void main(String[] args) {
		
		try {
			output_dir = System.getProperty(OUTPUT_DIR) != null ? System.getProperty(OUTPUT_DIR) : args[1];
		} catch (Exception e) {
			windowsUser = true;
		}

		// Permet d'accéder à toutes les fonctionnalités de Spark
		SparkSession spark = SparkSession
				  .builder()
				  .appName("Spark SQL TP1")
//				  .config("spark.master", "local[*]")
				  .getOrCreate();
		
		//
		// LES DATAFRAMES
		//
		System.out.println("**************");
		System.out.println("LES DATAFRAMES");
		System.out.println("**************");
		
		// Lecture du fichier d'entrée
		Dataset<Row> df = spark.read()
				.option("header", "true")
				.option("delimiter",DELIMITER)
				.csv(System.getProperty(INPUT_DIR) != null ? System.getProperty(INPUT_DIR) : args[0]);
		
		// Affichage des 100 premiers objet du contenu DataFrame
		System.out.println("Affichage des 100 premiers objet du contenu DataFrame");
		print(df, false, 100, "100 premiers objet");
		
		// Affichage les 100 premieres lignes et uniquement de la classe d'âge, de la nature de prestation et du montant de la prestation
		System.out.println("Affichage les 100 premieres lignes et uniquement de la classe d'âge, de la nature de prestation et du montant de la prestation");
		print(df.select("AGE_BEN_SNDS", "PRS_NAT", "PRS_PAI_MNT"), false, 100, "100 premieres lignes et uniquement de la classe d'âge, de la nature de prestation et du montant de la prestation");
		
		// TODO : Liste les paiements de plus de 100 k€
		System.out.println("Liste les paiements de plus de 100 k€");
		
		
		// TODO : Compte les paiements par nature de prestations
		System.out.println("Compte les paiements par nature de prestations");
		
		
		System.out.println("*****************");
		System.out.println("En SQL avec Spark");
		System.out.println("*****************");		
		
		// Et en SQL !
		// Création d'une vue SQL temporaire à partir du DataFrame
		df.createOrReplaceTempView("decomptes");
		// Liste les paiements de plus de 100 k€
		System.out.println("Liste les paiements de plus de 100 k€");
		print(spark.sql("select AGE_BEN_SNDS, PRS_NAT, PRS_PAI_MNT from decomptes where PRS_PAI_MNT > 100000"), true, 0, "SQL - paiements de plus de 100 k€");
		
		//
		// LES DATASETS
		//
		System.out.println("************");
		System.out.println("LES DATASETS");
		System.out.println("************");		
		
		//Pour la s&rialisation des objets dans la liste
		Encoder<Decompte> personEncoder = Encoders.bean(Decompte.class);
		Dataset<Decompte> ds = spark.read()
									.option("header", "true")
									.option("delimiter",DELIMITER)
									.csv(System.getProperty(INPUT_DIR) != null ? System.getProperty(INPUT_DIR) : args[0])
									.map(new MapFunction<Row, Decompte>() {

										private static final long serialVersionUID = 1L;

										@Override
										public Decompte call(Row row) throws Exception {
											
											Decompte decompte = new Decompte();
											decompte.setAge(Integer.valueOf(row.getString(ID_AGE_BEN_SNDS)));
											decompte.setMontant(Float.valueOf(row.getString(ID_PRS_PAI_MNT)));
											decompte.setNaturePrestation(row.getString(ID_PRS_NAT));

											return decompte;
										}
										
									}, personEncoder);
		
		System.out.println("Liste les paiements de plus de 100 k€");
		ds.filter(new FilterFunction<Decompte>() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public boolean call(Decompte value) throws Exception {
				return value.getMontant() > 100000;
			}}).show();
   	}
	
	public static void print(final Dataset<Row> dataframe, final boolean showAll, final int nbLineToShow, final String fileName) {
		if (windowsUser) {
			if (showAll) {
				dataframe.show();
			} else {
				dataframe.show(nbLineToShow);
			}
		} else {
			dataframe.write().csv(output_dir + fileName);
		}
	}
}