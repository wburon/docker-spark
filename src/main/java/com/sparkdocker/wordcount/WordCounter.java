package com.sparkdocker.wordcount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;

public class WordCounter {

    @SuppressWarnings({ "rawtypes", "unchecked" })
	private static void wordCount(String fileName) {

        SparkConf sparkConf = new SparkConf().setAppName("TP Word Count")
        		// Execution local
//        		.setMaster("local[*]")
        		;

        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        
        // Load file
        JavaRDD<String> inputFile = sparkContext.textFile(fileName);

        // TODO : Split file by word
        // JavaDoc : https://spark.apache.org/docs/latest/api/java/org/apache/spark/rdd/RDD.html
        JavaRDD<String> wordsFromFile = ...;

        // TODO : Count words
        // Indice : Tuple2
        JavaPairRDD countData = ...;

        // Save result
        countData.saveAsTextFile("CountResult");
        
        sparkContext.close();
    }

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("No files provided.");
            System.exit(0);
        }

        wordCount(args[0]);
    }
}
