package com.sparkdocker.tp1;

import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class SparkTP1 {

	private static final String INPUT_DIR = "input.dir";
	private static final String OUTPUT_DIR = "output.dir";
	
	// Classe d'âge de l'assuré
	private static final int ID_AGE_BEN_SNDS = 2;
	// Nature de la prestation
	private static final int ID_PRS_NAT = 39;
	// Montant de la prestation
	private static final int ID_PRS_PAI_MNT = 20;
	// Variable : age de l'assuré
	private static final String VAR_AGE_ASSURE = "AGE_BEN_SNDS";	
	
	public static JavaSparkContext sc = null;
	
	@SuppressWarnings({"serial", "rawtypes", "deprecation", "unchecked"})
	public static void main(String[] args) {
		
		final boolean windowsUser = args.length == 1;		
		
		// Création d'une appli Spark nommée "Docker Spark TP1"
        SparkConf conf = new SparkConf().setAppName("Docker Spark TP1")
        		// Execution local
//        		.setMaster("local[*]")
        		;
        
        sc = new JavaSparkContext(conf);
 
        // Création du RDD : lecture du fichier
        JavaRDD<String> lines = sc.textFile(System.getProperty(INPUT_DIR) != null ? System.getProperty(INPUT_DIR) : args[0])
        		.filter(x -> !x.contains("AGE_BEN_SNDS")); // Suppression de la ligne d'entête

        // Regroupement des classes
        JavaPairRDD<String, Float> prestations = lines.mapToPair(
        		new PairFunction<String, String, Float>() {

					@Override
					public Tuple2<String, Float> call(String line) throws Exception {
					
						Tuple2<String, Float> ret = null;	
						String age = null;
						String naturePrestation = null;
						Float montant = null;
						// On ignore l'entête : Si une ligne contient "AGE_BEN_SNDS" c'est qu'il s'agit de l'entête
						if (!line.contains(VAR_AGE_ASSURE)){
							String[] values = line.split(";");
							// On regroupe les montants remboursés pour chaque nature de prestation et classe d'âge
							age = values[ID_AGE_BEN_SNDS];
							naturePrestation = values[ID_PRS_NAT];
							montant = StringUtils.isNotEmpty(values[ID_PRS_PAI_MNT]) ? Float.parseFloat(values[ID_PRS_PAI_MNT]) : 0;
							
							ret = new Tuple2<String, Float>(age+"_"+naturePrestation, montant);
						}						
												
						return ret;
					}
        		}
        );

        //
        // MOYENNE
        //
        
        // Pour chaque valeur, on retourne en plus de la valeur, un compteur égal à 1 qui sera additionné ensuite pour 
        // calculer le nombre d'élements par catégorie
        JavaPairRDD<String, Float> resultsMeanRDD = prestations.mapValues(
        		
        		new Function<Float, Tuple2<Float, Integer>>() {

					@Override
					public Tuple2<Float, Integer> call(Float t) throws Exception {
						return new Tuple2<Float, Integer>(t, 1);
					}
        		})
        // Pour chaque catégorie, la fonction reduce permet d'effectuer l'addition des valeurs et des compteurs
        .reduceByKey(
        		new Function2<Tuple2<Float, Integer>, Tuple2<Float, Integer>, Tuple2<Float, Integer>> () {

					@Override
					public Tuple2<Float, Integer> call(Tuple2<Float, Integer> v1, Tuple2<Float, Integer> v2)
							throws Exception {
						return new Tuple2<Float, Integer>(v1._1()+v2._1(), v1._2()+v2._2());
					}
       		})
        // Pour chaque catégorie réduite par le calcul précedent, on divise le montant par le nombre pour déterminer le montant moyen
        .mapValues(
        		new Function<Tuple2<Float, Integer>, Float> (){

					@Override
					public Float call(Tuple2<Float, Integer> v1) throws Exception {
						return v1._1()/v1._2();
					}
        			
        		}
        );

        //
        // TODO : MIN
        //
        // Pour chaque cat�gorie, on applique la fonction min pour déterminer la valeur minimale de la classe Age/Prestation

        		
        //
        // TODO : MAX
        //
        // Pour chaque catégorie, on applique la fonction min pour déterminer la valeur maximale de la classe Age/Prestation

        
        //
        // TODO : ECART-TYPE
        //
        
        // Pour chaque valeur, on retourne en plus de la valeur, un compteur égal à 1 qui sera additionné ensuite pour 
        // calculer le nombre d'élements par catégorie


        
        /*
         * Affichage pour le sutilisateur de Windows 
         */
        if (windowsUser) {
	        // Affichage "Moyenne"
	        List<Tuple2<String, Float>> means = resultsMeanRDD.collect();
	        means.stream().forEach(new Consumer<Tuple2<String, Float>>() {
	
				@Override
				public void accept(Tuple2<String, Float> t) {
					System.out.println("Moyenne ("+t._1()+") : "+t._2());
				}
	        });
	        
	        // Affichage "Min"
//	        List<Tuple2<String, Float>> mins = resultsMinRDD.collect();
//	        mins.stream().forEach(new Consumer<Tuple2<String, Float>>() {
//	
//				@Override
//				public void accept(Tuple2<String, Float> t) {
//					System.out.println("Min ("+t._1()+") : "+t._2());
//				}
//	        });
	        
	        // Affichage "Max"
//	        List<Tuple2<String, Float>> maxs = resultsMaxRDD.collect();
//	        maxs.stream().forEach(new Consumer<Tuple2<String, Float>>() {
//	
//				@Override
//				public void accept(Tuple2<String, Float> t) {
//					System.out.println("Max ("+t._1()+") : "+t._2());
//				}
//	        });        
	        
	        // Affichage "Ecart-type"
//	        List<Tuple2<String, Double>> stdDevs = resultsStdRDD.collect();
//	        stdDevs.stream().forEach(new Consumer<Tuple2<String, Double>>() {
//	
//				@Override
//				public void accept(Tuple2<String, Double> t) {
//					System.out.println("StdDev ("+t._1()+") : "+t._2());
//				}
//	        });
        }
        else {
        
	        /*
	         * Sauvegarde dans des fichiers pour les utilisateurs de Linux
	         */
	        
	        // Sauvegarde du fichier "Moyenne" en sortie
	        resultsMeanRDD.saveAsTextFile(System.getProperty(OUTPUT_DIR) != null ? System.getProperty(OUTPUT_DIR) : args[1]);
	        // Sauvegarde du fichier "Min" en sortie
//	        resultsMinRDD.saveAsTextFile(System.getProperty(OUTPUT_DIR) != null ? System.getProperty(OUTPUT_DIR) : args[2]);        
	        // Sauvegarde du fichier "Max" en sortie
//	        resultsMaxRDD.saveAsTextFile(System.getProperty(OUTPUT_DIR) != null ? System.getProperty(OUTPUT_DIR) : args[3]);
	        // Sauvegarde du fichier "Ecart-type" en sortie
//	        resultsStdRDD.saveAsTextFile(System.getProperty(OUTPUT_DIR) != null ? System.getProperty(OUTPUT_DIR) : args[4]);
        }
   	}


}
