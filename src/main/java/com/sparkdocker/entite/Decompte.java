package com.sparkdocker.entite;

public class Decompte {

	private int age;
	private String naturePrestation;
	private float montant;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getNaturePrestation() {
		return naturePrestation;
	}
	public void setNaturePrestation(String naturePrestation) {
		this.naturePrestation = naturePrestation;
	}
	public float getMontant() {
		return montant;
	}
	public void setMontant(float montant) {
		this.montant = montant;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + Float.floatToIntBits(montant);
		result = prime * result + ((naturePrestation == null) ? 0 : naturePrestation.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Decompte other = (Decompte) obj;
		if (age != other.age)
			return false;
		if (Float.floatToIntBits(montant) != Float.floatToIntBits(other.montant))
			return false;
		if (naturePrestation == null) {
			if (other.naturePrestation != null)
				return false;
		} else if (!naturePrestation.equals(other.naturePrestation))
			return false;
		return true;
	}
	
	
}
